const express = require('express');
const app = express();
const session = require('express-session');
const {
  index,
  login,
  loginPost,
  logout,
} = require('./controllers/authController');
const {
  addUserView,
  addUser,
  getUser,
  deleteUserGameBiodata,
  updateUserGame,
  updateUserGamePost,
} = require('./controllers/userController');
const {
  dashboard,
  history,
  addAdmin,
  createAdmin,
  deleteUserAdmin,
  updateAdmin,
  updateAdminPost,
} = require('./controllers/adminController');

// Import routers
const userApiRoute = require('./routes/userApiRoute');

app.use(express.json());

app.use(
  session({
    secret: 'nabhan',
    name: 'uniqueSessionID',
    saveUninitialized: false,
  })
);

app.use(
  express.urlencoded({
    extended: false,
  })
);

app.use(express.static('public'));

app.set('view engine', 'ejs');

// It will go to index function in authController when user get request to http://localhost:3000/
app.get('/', index);

// Auth
app.get('/login', login);
app.get('/logout', logout);
app.post('/login', loginPost);

// Admin
app.get('/dashboard', dashboard);
app.get('/history', history);
app.get('/admin/add', addAdmin);
app.post('/admin/save', createAdmin);
app.get('/admin/delete/:id', deleteUserAdmin);
app.get('/admin/update/:id', updateAdmin);
app.post('/admin/update/:id', updateAdminPost);

// User
app.get('/user', getUser);
app.get('/user/add', addUserView);
app.post('/user/save', addUser);
app.get('/user/delete/:id', deleteUserGameBiodata);
app.get('/user/update/:id', updateUserGame);
app.post('/user/update/:id', updateUserGamePost);

// API Routes
app.use('/api/user', userApiRoute);

app.listen(3000, () => console.log('apps sudah jalan di port 3000'));
