const { user_admin } = require('../models');

module.exports = {
  index: (req, res) => {
    res.redirect('/login');
  },
  login: (req, res) => {
    res.render('login_view');
  },
  loginPost: (req, res) => {
    const { username, password } = req.body;

    user_admin
      .findAll({
        where: {
          username: username,
          password: password,
        },
      })
      .then((userGame) => {
        if (userGame.length === 0) {
          return res.redirect(301, '/login');
        }
        req.session.loggedIn = true;
        res.redirect('/dashboard');
      });
  },
  logout: (req, res) => {
    req.session.loggedIn = false;
    res.render('login_view');
  },
};
