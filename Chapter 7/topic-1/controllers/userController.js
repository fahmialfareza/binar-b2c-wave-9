const { user_admin, user_game, user_game_biodata } = require('../models');

module.exports = {
  addUserView: (req, res) => {
    res.render('addUserView');
  },
  addUser: (req, res) => {
    user_game
      .create({
        username: req.body.username,
        password: req.body.password,
        cash: req.body.cash,
        level: req.body.level,
      })
      .then((userGame) => {
        user_game_biodata
          .create({
            namaLengkap: req.body.namaLengkap,
            email: req.body.email,
            alamat: req.body.alamat,
            noTelpon: req.body.noTelpon,
            urlFoto: req.body.urlFoto,
          })
          .then((biodata) => res.redirect(301, '/dashboard'));
      });
  },
  getUser: (req, res) => {
    if (req.session.loggedIn) {
      user_game
        .findAll()
        .then((userGame) =>
          user_admin
            .findAll()
            .then((userAdmin) =>
              res.render('user_view', { userGame, userAdmin })
            )
        );
    } else {
      res.redirect('/login');
    }
  },
  deleteUserGameBiodata: (req, res) => {
    const userId = req.params.id;

    user_game_biodata
      .destroy({
        where: {
          id: userId,
        },
      })
      .then((biodata) => {
        user_game
          .destroy({
            where: {
              id: userId,
            },
          })
          .then((user) => {
            res.redirect(301, '/dashboard');
          });
      });
  },
  updateUserGame: (req, res) => {
    const userId = req.params.id;

    user_game
      .findOne({
        where: {
          id: userId,
        },
      })
      .then((user) => {
        user_game_biodata
          .findOne({
            where: {
              id: user.id,
            },
          })
          .then((biodata) => {
            res.render('updateUserView', { user, biodata });
          });
      });
  },
  updateUserGamePost: (req, res) => {
    const userId = req.params.id;

    user_game
      .update(
        {
          username: req.body.username,
          password: req.body.password,
          cash: req.body.cash,
          level: req.body.level,
        },
        {
          where: {
            id: userId,
          },
        }
      )
      .then((user) => {
        user_game_biodata
          .update(
            {
              namaLengkap: req.body.namaLengkap,
              email: req.body.email,
              alamat: req.body.alamat,
              noTelpon: req.body.noTelpon,
              urlFoto: req.body.urlFoto,
            },
            {
              where: {
                id: userId,
              },
            }
          )
          .then((biodata) => {
            res.redirect(301, '/dashboard');
          });
      });
  },
};
