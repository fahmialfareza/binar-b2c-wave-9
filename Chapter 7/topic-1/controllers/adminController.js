const {
  user_admin,
  user_game,
  user_game_biodata,
  user_game_history,
} = require('../models');

module.exports = {
  dashboard: (req, res) => {
    if (req.session.loggedIn) {
      user_game.findAll().then((userGame) =>
        user_game_biodata.findAll().then((userBio) =>
          user_game_history.findAll().then((userHistory) =>
            user_admin.findAll().then((userAdmin) =>
              res.render('dashboard_view', {
                userGame,
                userBio,
                userHistory,
                userAdmin,
              })
            )
          )
        )
      );
    } else {
      res.redirect('/login');
    }
  },
  history: (req, res) => {
    if (req.session.loggedIn) {
      user_game_history
        .findAll()
        .then((userHistory) => res.render('history_view', { userHistory }));
    } else {
      res.redirect('/login');
    }
  },
  addAdmin: (req, res) => {
    res.render('addAdminView');
  },
  createAdmin: (req, res) => {
    user_admin
      .create({
        username: req.body.username,
        password: req.body.password,
      })
      .then((biodata) => res.redirect(301, '/user'));
  },
  deleteUserAdmin: (req, res) => {
    const adminId = req.params.id;
    user_admin
      .destroy({
        where: {
          id: adminId,
        },
      })
      .then((admin) => res.redirect(301, '/user'));
  },
  updateAdmin: (req, res) => {
    const adminId = req.params.id;
    user_admin
      .findOne({
        where: {
          id: adminId,
        },
      })
      .then((admin) => res.render('updateAdminView', { admin }));
  },
  updateAdminPost: (req, res) => {
    const adminId = req.params.id;
    user_admin
      .update(
        {
          username: req.body.username,
          password: req.body.password,
        },
        {
          where: {
            id: adminId,
          },
        }
      )
      .then((admin) => res.redirect(301, '/user'));
  },
};
