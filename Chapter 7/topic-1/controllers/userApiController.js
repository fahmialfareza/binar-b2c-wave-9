const { user_game, user_game_biodata } = require('../models');

module.exports = {
  getAllUserGameBiodata: (req, res) => {
    user_game_biodata.findAll().then((userGameBiodata) => {
      res.status(200).json(userGameBiodata);
    });
  },
  getAllUserGame: (req, res) => {
    user_game.findAll().then((userGame) => {
      res.status(200).json(userGame);
    });
  },
  updateUser: (req, res) => {
    const userId = req.params.id;

    user_game
      .update(
        {
          username: req.body.username,
          password: req.body.password,
          cash: req.body.cash,
          level: req.body.level,
        },
        {
          where: {
            id: userId,
          },
        }
      )
      .then((user) => {
        user_game_biodata
          .update(
            {
              namaLengkap: req.body.namaLengkap,
              email: req.body.email,
              alamat: req.body.alamat,
              noTelpon: req.body.noTelpon,
              urlFoto: req.body.urlFoto,
            },
            {
              where: {
                id: userId,
              },
            }
          )
          .then((biodata) => {
            res.status(200).json({ message: 'berhasil update data' });
          });
      });
  },
};
