const express = require('express');
const router = express.Router(); // Import router

// Import controller
const {
  getAllUserGame,
  updateUser,
  getAllUserGameBiodata,
} = require('../controllers/userApiController');

// The first way to route
// router.get('/', getAllUserGame);

// If there are same endpoints and different method
router.route('/').get(getAllUserGame).post(getAllUserGameBiodata);

// It will go to updateUser function in userApiController when user post request to http://localhost:3000/api/user/update/:id
router.post('/update/:id', updateUser);

module.exports = router;
