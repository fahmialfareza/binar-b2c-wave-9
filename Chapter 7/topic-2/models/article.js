'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class article extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.article.belongsTo(models.publisher, {
        foreignKey: 'id_publisher',
      });
    }
  }
  article.init(
    {
      id_publisher: DataTypes.INTEGER,
      title: DataTypes.STRING,
      body: DataTypes.STRING,
      approved: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: 'article',
    }
  );
  return article;
};
