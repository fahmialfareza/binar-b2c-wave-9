const { article, publisher } = require('../models');

exports.home = (req, res) => {
  const title = 'Hello Ganov',
    subTitle = 'Welcome to the world!',
    azhary = 'Woy';

  res.render('index', { title, subTitle, azhary });
};

exports.about = (req, res) => {
  res.render('about', { title: 'About' });
};

// Promise
// exports.articles = (req, res) => {
//   article
//     .findAll({
//       include: [{ model: publisher }],
//     })
//     .then((articles) => {
//       return [articles, publisher.findAll()];
//     }).then(data => {
//       res.render('articles', { title: 'Articles', data[0] });
//     })
// };

// Async-Await
exports.articles = async (req, res) => {
  const articles = await article.findAll({ include: [{ model: publisher }] });
  const publishers = await publisher.findAll();

  res.render('articles', { title: 'Articles', articles, publishers });
};
