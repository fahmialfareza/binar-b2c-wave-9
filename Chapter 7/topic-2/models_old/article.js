const articles = [
  {
    id: 1,
    title: 'What is Lorem Ipsum?',
    body: `Lorem Ipsum is simply dummy text of the printing and typesetting industry.`,
    address: 'Jakarta',
    approved: true,
  },
  {
    id: 2,
    title: 'Why do we use it?',
    body: `It is a long established fact.`,
    address: 'Surakarta',
    approved: true,
  },
  {
    id: 3,
    title: 'Hello World',
    body: 'Cuma lorem ipsum aja kok',
    address: 'Yogyakarta',
    approved: false,
  },
  {
    id: 4,
    title: 'Muhammad Nabhan Naufal?',
    body: 'Jonathan Eduardus',
    address: 'Bandung',
    approved: true,
  },
];
module.exports = {
  findAll: () => Promise.resolve(articles),
  create: ({ title, body }) => {
    const id = articles[articles.length - 1].id + 1;
    const article = { id, title, body };
    articles.push(article);
    return Promise.resolve(article);
  },
};
