'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'publishers',
      [
        {
          name: 'John Doe',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Steve Doe',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Jonathan Doe',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('publishers', null, {});
  },
};
