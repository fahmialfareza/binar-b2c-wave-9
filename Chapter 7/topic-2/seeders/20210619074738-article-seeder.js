'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'articles',
      [
        {
          id_publisher: 3,
          title: 'This is one',
          body: 'lorem ipsum dolor sit amet',
          approved: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id_publisher: 4,
          title: 'This is two',
          body: 'lorem ipsum dolor sit amet',
          approved: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id_publisher: 5,
          title: 'This is three',
          body: 'lorem ipsum dolor sit amet',
          approved: false,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('articles', null, {});
  },
};
